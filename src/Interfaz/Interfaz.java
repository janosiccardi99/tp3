package Interfaz;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import Negocio.CentroDistribucion;
import Negocio.Cliente;
import Negocio.Instancia;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class Interfaz extends JFrame {
	Instancia instancia;
	private static final long serialVersionUID = 1L;
	public JFrame frame;
	private JPanel panelMapa;
	private JMapViewer mapa;
	ArrayList<CentroDistribucion> centros;
	ArrayList<Cliente> clientes;
	ArrayList<CentroDistribucion> centrosPosibles;
	private MapPolygonImpl camino;
	HashMap<Cliente, CentroDistribucion> set;
	boolean aceptar = true;
	private ArrayList<MapPolygonImpl> losCaminos;
	String _c1;
	String _c2;

	public Interfaz(String c1, String c2) {
		_c1 = c1;
		_c2 = c2;
		instancia = new Instancia(c1, c2);
		centros = instancia.getCentros();
		clientes = instancia.getClientes();
		frame = new JFrame();
		frame.setBounds(500, 150, 831, 743);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panelMapa = new JPanel();
		panelMapa.setBounds(10, 93, 795, 600);
		frame.getContentPane().add(panelMapa);

		mapa = new JMapViewer();
		mapa.setBounds(0, 0, 795, 599);
		Coordinate corde = new Coordinate(-34.521, -58.7008);
		mapa.setDisplayPosition(corde, 13);
		panelMapa.setLayout(null);

		panelMapa.add(mapa);

		JLabel lblNewLabel = new JLabel("Cantidad de centros que desea abrir:");
		lblNewLabel.setBounds(265, 28, 185, 14);
		frame.getContentPane().add(lblNewLabel);

		dibujarCentros(centros, Color.RED);
		dibujarClientes();

		JSpinner K = new JSpinner();
		K.setBounds(447, 22, 35, 23);
		K.setModel(new SpinnerNumberModel(1, 1, centros.size(), 1));

		frame.getContentPane().add(K);

		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(275, 53, 99, 23);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aceptar) {
					centrosPosibles = instancia.solucion((int) K.getValue());
					dibujarCentros(centrosPosibles, Color.GREEN);
					instancia.guardarSolucion();
					dibujarCamino(instancia.getCentroPorCliente());
					JOptionPane.showMessageDialog(null, "El costo total es: " + instancia.calcularCostoTotal());
					aceptar = false;
				} else {
					JOptionPane.showMessageDialog(null, "Se debe limpiar el mapa");
				}
			}
		});
		frame.getContentPane().add(btnAceptar);

		JButton btnLimpiar = new JButton("Limpiar Mapa");
		btnLimpiar.setBounds(395, 53, 101, 23);
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
			}
		});
		frame.getContentPane().add(btnLimpiar);
		
		JLabel lblNewLabel_1 = new JLabel("Centros cerrados");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(29, 57, 117, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Clientes");
		lblNewLabel_1_1.setForeground(Color.BLUE);
		lblNewLabel_1_1.setBounds(29, 15, 117, 14);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Centros abiertos");
		lblNewLabel_1_2.setForeground(new Color(50, 205, 50));
		lblNewLabel_1_2.setBounds(29, 36, 117, 14);
		frame.getContentPane().add(lblNewLabel_1_2);

	}

	public void dibujarCentros(ArrayList<CentroDistribucion> centros, Color c) {
		for (CentroDistribucion centro : centros) {
			marcarPunto(centro.get_latitud(), centro.get_longitud(), c);
		}
	}

	public void dibujarClientes() {
		for (Cliente cliente : clientes) {
			marcarPunto(cliente.get_latitud(), cliente.get_longitud(), Color.BLUE);
		}
	}

	private void marcarPunto(double x, double y, Color c) {
		Coordinate markeradd = new Coordinate(x, y);
		MapMarkerDot punto = new MapMarkerDot(markeradd);
		punto.getStyle().setBackColor(c);
		mapa.addMapMarker(punto);
	}

	private void dibujarCamino(HashMap<Cliente, CentroDistribucion> set) {
		losCaminos = new ArrayList<MapPolygonImpl>();
		for (Cliente c : set.keySet()) {
			Coordinate c1 = new Coordinate(c.get_latitud(), c.get_longitud());
			Coordinate c2 = new Coordinate(set.get(c).get_latitud(), set.get(c).get_longitud());
			camino = new MapPolygonImpl(c1, c2, c1);
			camino.setColor(Color.BLACK);
			losCaminos.add(camino);
			mapa.addMapPolygon(camino);

		}
	}

	public void reset() {
		instancia = new Instancia(_c1, _c2);
		centros = instancia.getCentros();
		clientes = instancia.getClientes();
		limpiarCaminos();
		limpiarPuntos();
		aceptar = true;
	}

	private void limpiarPuntos() {
		dibujarCentros(centrosPosibles, Color.RED);

	}

	private void limpiarCaminos() {

		for (MapPolygonImpl c : losCaminos) {
			mapa.removeMapPolygon(c);
		}

	}
}
