package Principal;

import java.awt.EventQueue;

import javax.swing.UIManager;

import Interfaz.Interfaz;

public class Main {

	public static void main(String[] args) {
		
		//NOMBRE DE ARCHIVOS JSON
		
		//OPCION 1
		String centros="centros.JSON";
		String clientes="clientes.JSON";
		
		//OPCION 2
		//String centros="centros2.JSON";
		//String clientes="clientes2.JSON";
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
				}
				try {
					Interfaz window = new Interfaz(centros, clientes);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
