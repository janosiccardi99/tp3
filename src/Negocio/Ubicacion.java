package Negocio;

abstract class Ubicacion {


	private double _latitud;
	private double _longitud;	
	
	public Ubicacion(double latitud, double longitud){
		_latitud = latitud;
		_longitud = longitud;
		
	}	
	
	public double get_latitud() {
		return _latitud;
	}

	public double get_longitud() {
		return _longitud;
	}
	
	@Override
	public String toString() {
		return "[latitud=" + _latitud + ", longitud=" + _longitud+ "]";
	}
	
	@Override
	public boolean equals(Object obj) {			
		Ubicacion other = (Ubicacion) obj;
		if (Double.doubleToLongBits(_latitud) != Double.doubleToLongBits(other._latitud) || (Double.doubleToLongBits(_longitud) != Double.doubleToLongBits(other._longitud)))
			return false;
		return true;
	}

}